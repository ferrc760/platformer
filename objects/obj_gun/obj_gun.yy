{
    "id": "bfebbf53-63f0-4433-a6cd-cbb6306b26c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun",
    "eventList": [
        {
            "id": "343f5801-2df6-4533-9534-32676892d8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bfebbf53-63f0-4433-a6cd-cbb6306b26c3"
        },
        {
            "id": "ed6f5ec8-ddcf-47bb-8b82-0e1b26667465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bfebbf53-63f0-4433-a6cd-cbb6306b26c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e304c7a5-3204-46f0-a61f-9a776eef331b",
    "visible": true
}