{
    "id": "79e61eb8-c65c-48f0-89c5-5c1d07570919",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "32fe6af4-7390-468c-94b1-09c4206d955f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79e61eb8-c65c-48f0-89c5-5c1d07570919"
        },
        {
            "id": "54200572-2c59-4c19-bc78-b7b9652af471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79e61eb8-c65c-48f0-89c5-5c1d07570919"
        },
        {
            "id": "211bb6af-9c7b-4e5b-99b5-e21ed9e8389c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "79e61eb8-c65c-48f0-89c5-5c1d07570919"
        }
    ],
    "maskSpriteId": "bc7b8412-dda1-44fe-9857-5d14269a0ffe",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bc7b8412-dda1-44fe-9857-5d14269a0ffe",
    "visible": true
}