{
    "id": "bc7b8412-dda1-44fe-9857-5d14269a0ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 18,
    "bbox_right": 42,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c6389a1-b4d3-42a6-b012-e81a1163086a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc7b8412-dda1-44fe-9857-5d14269a0ffe",
            "compositeImage": {
                "id": "d5725f3a-693f-40a2-a954-bb9d83ba4af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6389a1-b4d3-42a6-b012-e81a1163086a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b6d73e-7fd3-43af-92ad-6930e9147e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6389a1-b4d3-42a6-b012-e81a1163086a",
                    "LayerId": "d15503d0-2224-4cc6-995f-e10b10fd7c6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "d15503d0-2224-4cc6-995f-e10b10fd7c6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc7b8412-dda1-44fe-9857-5d14269a0ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}