{
    "id": "6dc8806f-16a1-443c-a122-5082a6fd6c98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 26,
    "bbox_right": 38,
    "bbox_top": 52,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7e2506c-e7ba-49f8-a931-15a378637cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc8806f-16a1-443c-a122-5082a6fd6c98",
            "compositeImage": {
                "id": "6872931b-ef34-4bad-9e9c-e8d4a6b480bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e2506c-e7ba-49f8-a931-15a378637cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5df1d2-512d-4357-9c87-c8cd802e7a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e2506c-e7ba-49f8-a931-15a378637cdd",
                    "LayerId": "a8f2f0ef-f1c2-4034-9ecb-752752b3e830"
                }
            ]
        },
        {
            "id": "19654bb6-549f-47da-a7eb-87a7c33d03f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc8806f-16a1-443c-a122-5082a6fd6c98",
            "compositeImage": {
                "id": "1def05f4-aeb5-4e6a-861c-b193f0430b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19654bb6-549f-47da-a7eb-87a7c33d03f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f84510-2300-4f5b-ac2d-3b16688ce8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19654bb6-549f-47da-a7eb-87a7c33d03f2",
                    "LayerId": "a8f2f0ef-f1c2-4034-9ecb-752752b3e830"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8f2f0ef-f1c2-4034-9ecb-752752b3e830",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dc8806f-16a1-443c-a122-5082a6fd6c98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 35,
    "yorig": 57
}