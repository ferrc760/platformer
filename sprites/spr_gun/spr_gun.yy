{
    "id": "e304c7a5-3204-46f0-a61f-9a776eef331b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0ec1415-f643-4bce-b32f-8a04d142e882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e304c7a5-3204-46f0-a61f-9a776eef331b",
            "compositeImage": {
                "id": "888d689b-1789-4164-8b28-b9c5fd92284f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ec1415-f643-4bce-b32f-8a04d142e882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be22c1ec-e485-4af4-8e1e-3438e1f03855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ec1415-f643-4bce-b32f-8a04d142e882",
                    "LayerId": "99df6070-296a-412f-acaf-d39eb2d8c288"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99df6070-296a-412f-acaf-d39eb2d8c288",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e304c7a5-3204-46f0-a61f-9a776eef331b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 12,
    "yorig": 15
}