{
    "id": "cad7a4cf-60ef-46de-b92d-8f842cc1dd3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d443f6c9-436c-49a9-a743-64f0c2a039fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cad7a4cf-60ef-46de-b92d-8f842cc1dd3c",
            "compositeImage": {
                "id": "7dba75c0-4a24-42ab-8669-211f0cdd1bc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d443f6c9-436c-49a9-a743-64f0c2a039fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ecc979-b0c4-426e-97a6-fd10e34f27a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d443f6c9-436c-49a9-a743-64f0c2a039fc",
                    "LayerId": "76e2921a-105c-4b19-b0da-5cfffb1bdbed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "76e2921a-105c-4b19-b0da-5cfffb1bdbed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cad7a4cf-60ef-46de-b92d-8f842cc1dd3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}