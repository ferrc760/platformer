{
    "id": "e565e382-6537-4158-a7d8-68cac9320d24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da547df6-2804-4d76-bf13-dcc42d6ca90d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e565e382-6537-4158-a7d8-68cac9320d24",
            "compositeImage": {
                "id": "25211ce5-7e29-4cd9-91ef-b34bbee8e47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da547df6-2804-4d76-bf13-dcc42d6ca90d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c1f4a2-0330-43cc-867c-fa36fef04396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da547df6-2804-4d76-bf13-dcc42d6ca90d",
                    "LayerId": "7222a76d-df17-4bbd-a0e5-4a4ec3e18461"
                }
            ]
        },
        {
            "id": "a1b77995-0a65-49dd-a6e6-e0e232671865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e565e382-6537-4158-a7d8-68cac9320d24",
            "compositeImage": {
                "id": "a185bb6b-b51a-4383-bb80-899b9b3a13ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b77995-0a65-49dd-a6e6-e0e232671865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f25cf6d2-2a0d-49f5-825d-f6a96a5c4cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b77995-0a65-49dd-a6e6-e0e232671865",
                    "LayerId": "7222a76d-df17-4bbd-a0e5-4a4ec3e18461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7222a76d-df17-4bbd-a0e5-4a4ec3e18461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e565e382-6537-4158-a7d8-68cac9320d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 22,
    "yorig": 15
}