{
    "id": "bb89b2c1-f7c4-4b59-8420-c1dc03bf09fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 12,
    "bbox_right": 46,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d19dd45b-185b-4dd8-955a-cbb88ce9582d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb89b2c1-f7c4-4b59-8420-c1dc03bf09fb",
            "compositeImage": {
                "id": "2e0ec427-dd88-41eb-bc97-76f859e57701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19dd45b-185b-4dd8-955a-cbb88ce9582d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3599fc3-b262-4982-9925-24c29fbc84ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19dd45b-185b-4dd8-955a-cbb88ce9582d",
                    "LayerId": "57b5712a-34ca-4967-a421-191039d06219"
                }
            ]
        },
        {
            "id": "be257fc3-cc3f-4b31-a590-93ad3ace2326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb89b2c1-f7c4-4b59-8420-c1dc03bf09fb",
            "compositeImage": {
                "id": "f3df7435-c726-4b7d-862d-f60141e17f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be257fc3-cc3f-4b31-a590-93ad3ace2326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cfb89ee-9a5d-4d25-b390-4bf14e2b959b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be257fc3-cc3f-4b31-a590-93ad3ace2326",
                    "LayerId": "57b5712a-34ca-4967-a421-191039d06219"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "57b5712a-34ca-4967-a421-191039d06219",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb89b2c1-f7c4-4b59-8420-c1dc03bf09fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}