{
    "id": "1554c502-7e0d-421a-b654-f94177108f76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemiesA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "174801c5-04d0-4f5c-90e8-dca8ccf15e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1554c502-7e0d-421a-b654-f94177108f76",
            "compositeImage": {
                "id": "3b033352-2b96-4328-b733-884341810f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174801c5-04d0-4f5c-90e8-dca8ccf15e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4d3ea7-63ef-4080-8d73-e86d668b2332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174801c5-04d0-4f5c-90e8-dca8ccf15e78",
                    "LayerId": "2cf317b6-3c7b-4784-a045-1412c3f45811"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2cf317b6-3c7b-4784-a045-1412c3f45811",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1554c502-7e0d-421a-b654-f94177108f76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}