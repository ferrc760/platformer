{
    "id": "74d61a19-74c5-4a16-89e0-f72353dd4058",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemies",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 18,
    "bbox_right": 42,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0d1e2a6-3efe-493f-ba0a-ddc126175aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d61a19-74c5-4a16-89e0-f72353dd4058",
            "compositeImage": {
                "id": "59bde3fc-f855-4319-aed8-425d7e573bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d1e2a6-3efe-493f-ba0a-ddc126175aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "398d8740-2af1-45d0-befc-534ee12476e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d1e2a6-3efe-493f-ba0a-ddc126175aab",
                    "LayerId": "db4bb0f6-7540-4488-a757-85b33498f0b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "db4bb0f6-7540-4488-a757-85b33498f0b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74d61a19-74c5-4a16-89e0-f72353dd4058",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}