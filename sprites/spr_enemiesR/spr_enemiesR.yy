{
    "id": "1ce59ece-6dfe-48be-a770-b503c6c74122",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemiesR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 12,
    "bbox_right": 46,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ddf4fcc-983c-4137-9bc3-49fd74ef63af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ce59ece-6dfe-48be-a770-b503c6c74122",
            "compositeImage": {
                "id": "df5e4d5b-5ed1-48e5-9f14-a390f730e681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddf4fcc-983c-4137-9bc3-49fd74ef63af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb1687e9-24ea-47aa-b630-c4d23c22781b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddf4fcc-983c-4137-9bc3-49fd74ef63af",
                    "LayerId": "b8be37bb-4b42-4260-bc58-e9ef4ce63322"
                }
            ]
        },
        {
            "id": "745ba9cf-237c-4013-8f8c-58ff4cc0ac3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ce59ece-6dfe-48be-a770-b503c6c74122",
            "compositeImage": {
                "id": "0baf93e8-ec9e-4823-93af-02ff0792f4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745ba9cf-237c-4013-8f8c-58ff4cc0ac3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8291f2bc-efe7-4082-81eb-f404a0d589bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745ba9cf-237c-4013-8f8c-58ff4cc0ac3c",
                    "LayerId": "b8be37bb-4b42-4260-bc58-e9ef4ce63322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "b8be37bb-4b42-4260-bc58-e9ef4ce63322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ce59ece-6dfe-48be-a770-b503c6c74122",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}