{
    "id": "aed5290d-e256-46f7-993c-0f8e3254ff48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 18,
    "bbox_right": 42,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61301964-363e-4a26-b3cc-d53f4da0df4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed5290d-e256-46f7-993c-0f8e3254ff48",
            "compositeImage": {
                "id": "b8b1084a-826f-49f9-b653-cc56da74c30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61301964-363e-4a26-b3cc-d53f4da0df4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a37374f-2cff-4bbf-96ed-524654017b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61301964-363e-4a26-b3cc-d53f4da0df4f",
                    "LayerId": "adc868cb-da1a-46f7-8077-e9f705f38ea9"
                }
            ]
        },
        {
            "id": "08f36046-b6e1-4920-bbf0-651b078a2168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed5290d-e256-46f7-993c-0f8e3254ff48",
            "compositeImage": {
                "id": "483ce708-aeb8-4cf9-a0de-2e96c8bd03db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f36046-b6e1-4920-bbf0-651b078a2168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac94d1a-edaf-42ed-a446-edd043cf9f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f36046-b6e1-4920-bbf0-651b078a2168",
                    "LayerId": "adc868cb-da1a-46f7-8077-e9f705f38ea9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "adc868cb-da1a-46f7-8077-e9f705f38ea9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aed5290d-e256-46f7-993c-0f8e3254ff48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}